package com;

import com.mall.cz.tasks.*;
import cz.cvut.fel.still.sqa.seleniumStarterPack.config.DriverFactory;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.hasItem;
import static com.mall.cz.questions.RegisterOkQuestion.theDisplayedRegisterOkMessage;
import static com.mall.cz.questions.RegisterNokQuestion.theDisplayedRegisterNokMessage;
import static com.mall.cz.questions.LoginOkQuestion.theDisplayedLoginOkMessage;
import static com.mall.cz.questions.LoginIncorrectCredentialsQuestion.theDisplayedLoginIncorrectCredentialsNokMessage;
import static com.mall.cz.questions.ChangeAccountInfoOkQuestion.theDisplayedAccountInfoChangedOkMessage;
import static com.mall.cz.questions.ChangeAccountInfoNokQuestion.theDisplayedWarningMessage;
import static com.mall.cz.questions.AddToCartOkQuestion.theDisplayedCartFilledSummary;
import static com.mall.cz.questions.ChangePasswordOkQuestion.theDisplayedSuccessMessage;

@RunWith(SerenityRunner.class)
public class MainTest {

    private WebDriver browser;

    Actor ali = Actor.named("Ali");


    @Before
    public void before() throws IOException {
        browser = new DriverFactory().getDriver();
        givenThat(ali).can(BrowseTheWeb.with(new DriverFactory().getDriver()));
    }

    @After
    public void closeBrowser() {
        browser.close();
    }

    @Test
    public void testRegisterCustomer_WithPolicies_Ok() {
        String expectedText = "Your Account Has Been Created!";
        givenThat(ali).wasAbleTo(RegistrationInitiator.registrationPage());
        when(ali).attemptsTo(RegisterOk.called());
        then(ali).should(seeThat(theDisplayedRegisterOkMessage(), hasItem(expectedText)));
    }

    @Test
    public void testRegisterCustomer_WithoutPolicies_Nok() {
        String expectedText = "Warning: You must agree to the Privacy Policy!";
        givenThat(ali).wasAbleTo(RegistrationInitiator.registrationPage());
        when(ali).attemptsTo(RegisterBlankNok.called());
        then(ali).should(seeThat(theDisplayedRegisterNokMessage(), hasItem(expectedText)));
    }

    @Test
    public void testLoginCustomer_CorrectCredentials_Ok() {
        String expectedText = "My Orders";
        givenThat(ali).wasAbleTo(LoginInitiator.loginPage());
        when(ali).attemptsTo(LoginOk.called());
        then(ali).should(seeThat(theDisplayedLoginOkMessage(), hasItem(expectedText)));
    }

    @Test
    public void testLoginCustomer_IncorrectCredentials_Nok() {
        String expectedText = "Warning: No match for E-Mail Address and/or Password.";
        givenThat(ali).wasAbleTo(LoginInitiator.loginPage());
        when(ali).attemptsTo(LoginIncorrectCredentialsNok.called());
        then(ali).should(seeThat(theDisplayedLoginIncorrectCredentialsNokMessage(), hasItem(expectedText)));
    }

    @Test
    public void testChangeAccountInfo_ValidInput_Ok() {
        // Always change data
        String expectedText = "Success: Your account has been successfully updated.";
        givenThat(ali).wasAbleTo(LoginInitiator.loginPage());
        givenThat(ali).wasAbleTo(LoginOkHelper.called());
        when(ali).attemptsTo(ChangeAccountInfoOk.called());
        then(ali).should(seeThat(theDisplayedAccountInfoChangedOkMessage(), hasItem(expectedText)));
    }

    @Test
    public void testChangeAccountInfo_InvalidInput_Nok() {
        // Always change data
        String expectedText = "First Name must be between 1 and 32 characters!";
        givenThat(ali).wasAbleTo(LoginInitiator.loginPage());
        givenThat(ali).wasAbleTo(LoginOkHelper.called());
        when(ali).attemptsTo(ChangeAccountInfoNok.called());
        then(ali).should(seeThat(theDisplayedWarningMessage(), hasItem(expectedText)));
    }

    @Test
    public void testAddToCart_Ok() throws InterruptedException {
        // Always change data
        String expectedText = "1 item(s) - $123.20";
        givenThat(ali).wasAbleTo(PurchaseInitiator.iphonePage());
        when(ali).attemptsTo(PurchaseIphone.called());
        Thread.sleep(1000);
        then(ali).should(seeThat(theDisplayedCartFilledSummary(), hasItem(expectedText)));
    }

    @Test
    public void testChangePassword_Matching_Ok() {
        String expectedText = "0 item(s) - $0.00";
        givenThat(ali).wasAbleTo(LoginInitiator.loginPage());
        givenThat(ali).wasAbleTo(LoginOkHelper.called());
        when(ali).attemptsTo(ChangePasswordOk.called());
        then(ali).should(seeThat(theDisplayedSuccessMessage(), hasItem(expectedText)));
    }
}

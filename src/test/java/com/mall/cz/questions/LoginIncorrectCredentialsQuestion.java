package com.mall.cz.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static com.mall.cz.pobjects.LoginPage.INCORRECT_CREDENTIALS_MESSAGE_CONTAINER;

public class LoginIncorrectCredentialsQuestion implements Question<List<String>> {

    public static Question<List<String>> theDisplayedLoginIncorrectCredentialsNokMessage() {
        return new LoginIncorrectCredentialsQuestion();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(INCORRECT_CREDENTIALS_MESSAGE_CONTAINER).viewedBy(actor).asList();
    }
}

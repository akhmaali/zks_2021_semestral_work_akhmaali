package com.mall.cz.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static com.mall.cz.pobjects.LoginPage.LOGIN_CONFIRMATION_ATTRIBUTE;

public class LoginOkQuestion implements Question<List<String>> {

    public static Question<List<String>> theDisplayedLoginOkMessage() {
        return new LoginOkQuestion();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(LOGIN_CONFIRMATION_ATTRIBUTE).viewedBy(actor).asList();
    }
}

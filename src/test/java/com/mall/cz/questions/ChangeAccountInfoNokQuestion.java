package com.mall.cz.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static com.mall.cz.pobjects.ChangeAccountInfoPage.WARNING_VALIDATION_CONTAINER;

public class ChangeAccountInfoNokQuestion implements Question<List<String>> {

    public static Question<List<String>> theDisplayedWarningMessage() {
        return new ChangeAccountInfoNokQuestion();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(WARNING_VALIDATION_CONTAINER).viewedBy(actor).asList();
    }
}

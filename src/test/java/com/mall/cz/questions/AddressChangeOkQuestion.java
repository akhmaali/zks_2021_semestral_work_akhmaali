package com.mall.cz.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static com.mall.cz.pobjects.RegistrationPage.REGISTRATION_OK_CONTAINER;

public class AddressChangeOkQuestion implements Question<List<String>> {

    public static Question<List<String>> theAddressOkMessage() {
        return new AddressChangeOkQuestion();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(REGISTRATION_OK_CONTAINER).viewedBy(actor).asList();
    }
}

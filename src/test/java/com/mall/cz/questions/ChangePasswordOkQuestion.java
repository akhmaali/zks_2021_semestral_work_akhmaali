package com.mall.cz.questions;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static com.mall.cz.pobjects.MyAccountPage.ACCOUNT_INFO_UPDATED_MESSAGE_CONTAINER;

public class ChangePasswordOkQuestion implements Question<List<String>> {

    public static Question<List<String>> theDisplayedSuccessMessage() {
        return new AddToCartOkQuestion();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(ACCOUNT_INFO_UPDATED_MESSAGE_CONTAINER).viewedBy(actor).asList();
    }
}

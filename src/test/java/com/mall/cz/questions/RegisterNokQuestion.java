package com.mall.cz.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static com.mall.cz.pobjects.RegistrationPage.PRIVACY_POLICY_FAIL_MESSAGE_CONTAINER;

public class RegisterNokQuestion implements Question<List<String>> {

    public static Question<List<String>> theDisplayedRegisterNokMessage() {
        return new RegisterNokQuestion();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(PRIVACY_POLICY_FAIL_MESSAGE_CONTAINER).viewedBy(actor).asList();
    }
}

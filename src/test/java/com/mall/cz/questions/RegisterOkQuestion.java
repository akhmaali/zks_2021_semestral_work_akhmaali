package com.mall.cz.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import java.util.List;

import static com.mall.cz.pobjects.RegistrationPage.ACCOUNT_CREATED_HEADER;

public class RegisterOkQuestion implements Question<List<String>> {

    public static Question<List<String>> theDisplayedRegisterOkMessage() {
        return new RegisterOkQuestion();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(ACCOUNT_CREATED_HEADER).viewedBy(actor).asList();
    }
}

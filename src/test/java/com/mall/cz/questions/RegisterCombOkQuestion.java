package com.mall.cz.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;
import static com.mall.cz.pobjects.RegistrationPage.REGISTRATION_OK_CONTAINER;

public class RegisterCombOkQuestion implements Question<List<String>> {

    public static Question<List<String>> theRegCombOkMessage() {
        return new RegisterCombOkQuestion();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(REGISTRATION_OK_CONTAINER).viewedBy(actor).asList();
    }
}

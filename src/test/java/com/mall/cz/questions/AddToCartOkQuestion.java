package com.mall.cz.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static com.mall.cz.pobjects.IphonePage.CART_SUMMARY;

public class AddToCartOkQuestion implements Question<List<String>> {

    public static Question<List<String>> theDisplayedCartFilledSummary() {
        return new AddToCartOkQuestion();
    }


    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(CART_SUMMARY).viewedBy(actor).asList();
    }
}

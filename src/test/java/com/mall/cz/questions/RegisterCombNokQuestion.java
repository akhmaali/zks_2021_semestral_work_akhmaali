package com.mall.cz.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;
import static com.mall.cz.pobjects.RegistrationPage.REGISTRATION_NOK_CONTAINER;

public class RegisterCombNokQuestion implements Question<List<String>> {

    public static Question<List<String>> theRegCombNokMessage() {
        return new RegisterCombNokQuestion();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(REGISTRATION_NOK_CONTAINER).viewedBy(actor).asList();
    }
}

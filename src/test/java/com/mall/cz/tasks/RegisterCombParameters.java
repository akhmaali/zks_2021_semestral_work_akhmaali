package com.mall.cz.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static com.mall.cz.pobjects.RegistrationPage.*;

public class RegisterCombParameters implements Task {

    private String fistName;

    private String lastName;

    private String email;

    private String phone;

    private String password;

    public RegisterCombParameters(String fistName, String lastName, String email, String phone, String password) {
        this.fistName = fistName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.password = password;
    }

    public static RegisterCombParameters called(String fistName, String lastName, String email, String phone, String password) {
        return Instrumented.instanceOf(RegisterCombParameters.class).withProperties(fistName, lastName, email, phone, password);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(fistName).into(FIRST_NAME_INPUT),
                Enter.theValue(lastName).into(LAST_NAME_INPUT),
                Enter.theValue(email).into(EMAIL_INPUT),
                Enter.theValue(phone).into(PHONE_INPUT),
                Enter.theValue(password).into(PASSWORD_INPUT),
                Enter.theValue(password).into(PASSWORD_CONFIRMATION_INPUT),
                Click.on(CONTINUE_BUTTON)
        );
    }
}

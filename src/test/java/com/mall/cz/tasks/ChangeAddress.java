package com.mall.cz.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static com.mall.cz.pobjects.ChangeAddressPage.FIRST_NAME_INPUT;
import static com.mall.cz.pobjects.ChangeAddressPage.LAST_NAME_INPUT;
import static com.mall.cz.pobjects.ChangeAddressPage.ADDRESS_INPUT;
import static com.mall.cz.pobjects.ChangeAddressPage.CITY_INPUT;
import static com.mall.cz.pobjects.ChangeAddressPage.POST_CODE_INPUT;
import static com.mall.cz.pobjects.ChangeAddressPage.CONFIRM_BUTTON;
import static com.mall.cz.pobjects.MyAccountPage.EDIT_ADDRESS_BUTTON;
import static com.mall.cz.pobjects.MyAccountPage.ADD_ADDRESS_BUTTON;

public class ChangeAddress implements Task {

    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String postCode;

    public ChangeAddress(String firstName, String lastName, String address, String city, String postCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.postCode = postCode;
    }

    public static ChangeAddress called(String firstName, String lastName, String address, String city, String postCode) {
        return Instrumented.instanceOf(ChangeAddress.class).withProperties(firstName, lastName, address, city, postCode);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
            Click.on(EDIT_ADDRESS_BUTTON),
            Click.on(ADD_ADDRESS_BUTTON),
            Enter.theValue(this.firstName).into(FIRST_NAME_INPUT),
            Enter.theValue(this.lastName).into(LAST_NAME_INPUT),
            Enter.theValue(this.address).into(ADDRESS_INPUT),
            Enter.theValue(this.city).into(CITY_INPUT),
            Enter.theValue(this.postCode).into(POST_CODE_INPUT),
            Click.on(CONFIRM_BUTTON)
        );
    }
}

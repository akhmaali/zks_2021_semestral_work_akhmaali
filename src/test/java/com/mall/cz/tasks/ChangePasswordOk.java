package com.mall.cz.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static com.mall.cz.pobjects.MyAccountPage.CHANGE_PASSWORD_BUTTON;
import static com.mall.cz.pobjects.ChangePasswordPage.PASSWORD_INPUT;
import static com.mall.cz.pobjects.ChangePasswordPage.REPEAT_PASSWORD_INPUT;
import static com.mall.cz.pobjects.ChangePasswordPage.CONTINUE_BUTTON;

public class ChangePasswordOk implements Task {

    public static ChangePasswordOk called() {
        return Instrumented.instanceOf(ChangePasswordOk.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(CHANGE_PASSWORD_BUTTON),
                Enter.theValue("12345678").into(PASSWORD_INPUT),
                Enter.theValue("12345678").into(REPEAT_PASSWORD_INPUT),
                Click.on(CONTINUE_BUTTON)
        );
    }
}

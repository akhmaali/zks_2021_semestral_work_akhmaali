package com.mall.cz.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static com.mall.cz.pobjects.ChangeAccountInfoPage.*;
import static com.mall.cz.pobjects.MyAccountPage.EDIT_ACCOUNT_BUTTON;

public class ChangeAccountInfoNok implements Task {

    public static ChangeAccountInfoNok called() {
        return Instrumented.instanceOf(ChangeAccountInfoNok.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(EDIT_ACCOUNT_BUTTON),
                Enter.theValue("").into(FIRST_NAME_INPUT),
                Enter.theValue("").into(LAST_NAME_INPUT),
                Enter.theValue("").into(EMAIL_INPUT),
                Enter.theValue("").into(PHONE_INPUT),
                Click.on(CONFIRM_BUTTON)
        );
    }
}

package com.mall.cz.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static com.mall.cz.pobjects.IphonePage.ADD_TO_CART_BUTTON;

public class PurchaseIphone implements Task {

    public static PurchaseIphone called() {
        return Instrumented.instanceOf(PurchaseIphone.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(ADD_TO_CART_BUTTON)
        );
    }
}

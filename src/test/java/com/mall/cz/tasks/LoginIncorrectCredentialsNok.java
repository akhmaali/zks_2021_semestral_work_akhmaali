package com.mall.cz.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static com.mall.cz.pobjects.LoginPage.EMAIL_INPUT;
import static com.mall.cz.pobjects.LoginPage.PASSWORD_INPUT;
import static com.mall.cz.pobjects.LoginPage.LOGIN_BUTTON;

public class LoginIncorrectCredentialsNok implements Task {

    public static LoginIncorrectCredentialsNok called() {
        return Instrumented.instanceOf(LoginIncorrectCredentialsNok.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue("doodle@doodle.com").into(EMAIL_INPUT),
                Enter.theValue("bubble123").into(PASSWORD_INPUT),
                Click.on(LOGIN_BUTTON)
        );
    }
}

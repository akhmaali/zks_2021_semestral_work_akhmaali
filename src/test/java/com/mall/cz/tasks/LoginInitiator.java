package com.mall.cz.tasks;

import com.mall.cz.pobjects.LoginPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class LoginInitiator implements Task {

    LoginPage loginPage;

    public static LoginInitiator loginPage() {
        return instrumented(LoginInitiator.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(loginPage)
        );
    }
}

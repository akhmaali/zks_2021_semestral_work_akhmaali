package com.mall.cz.tasks;

import com.mall.cz.pobjects.RegistrationPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class RegistrationInitiator implements Task {

    RegistrationPage registrationPage;

    public static RegistrationInitiator registrationPage() {
        return instrumented(RegistrationInitiator.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(registrationPage)
        );
    }
}

package com.mall.cz.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static com.mall.cz.pobjects.LoginPage.EMAIL_INPUT;
import static com.mall.cz.pobjects.LoginPage.PASSWORD_INPUT;
import static com.mall.cz.pobjects.LoginPage.LOGIN_BUTTON;

/**
 * For majority of test cases.
 */
public class LoginOk implements Task {

    public static LoginOk called() {
        return Instrumented.instanceOf(LoginOk.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue("perm.account12345@gmail.com").into(EMAIL_INPUT),
                Enter.theValue("12345678").into(PASSWORD_INPUT),
                Click.on(LOGIN_BUTTON)
        );
    }
}

package com.mall.cz.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static com.mall.cz.pobjects.RegistrationPage.*;

public class RegisterBlankNok implements Task {

    public static RegisterBlankNok called() {
        return Instrumented.instanceOf(RegisterBlankNok.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue("Foo").into(FIRST_NAME_INPUT),
                Enter.theValue("Bar").into(LAST_NAME_INPUT),
                Enter.theValue("foo.barca@gmail.com").into(EMAIL_INPUT),
                Click.on(CONTINUE_BUTTON)
        );
    }
}

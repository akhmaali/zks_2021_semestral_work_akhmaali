package com.mall.cz.tasks;

import net.bytebuddy.utility.RandomString;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static com.mall.cz.pobjects.MyAccountPage.EDIT_ACCOUNT_BUTTON;
import static com.mall.cz.pobjects.ChangeAccountInfoPage.FIRST_NAME_INPUT;
import static com.mall.cz.pobjects.ChangeAccountInfoPage.LAST_NAME_INPUT;
import static com.mall.cz.pobjects.ChangeAccountInfoPage.EMAIL_INPUT;
import static com.mall.cz.pobjects.ChangeAccountInfoPage.PHONE_INPUT;
import static com.mall.cz.pobjects.ChangeAccountInfoPage.CONFIRM_BUTTON;

public class ChangeAccountInfoOk implements Task {

    public static ChangeAccountInfoOk called() {
        return Instrumented.instanceOf(ChangeAccountInfoOk.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(EDIT_ACCOUNT_BUTTON),
                Enter.theValue("Changename").into(FIRST_NAME_INPUT),
                Enter.theValue("Changelastname").into(LAST_NAME_INPUT),
                Enter.theValue("demo.account123456@gmail.com").into(EMAIL_INPUT),
                Enter.theValue("778999787").into(PHONE_INPUT),
                Click.on(CONFIRM_BUTTON)
        );
    }
}

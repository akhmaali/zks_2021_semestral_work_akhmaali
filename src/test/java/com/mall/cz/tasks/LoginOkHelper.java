package com.mall.cz.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static com.mall.cz.pobjects.LoginPage.*;

/**
 * For minority of test cases.
 */
public class LoginOkHelper implements Task {

    public static LoginOkHelper called() {
        return Instrumented.instanceOf(LoginOkHelper.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue("demo.account123456@gmail.com").into(EMAIL_INPUT),
                Enter.theValue("12345678").into(PASSWORD_INPUT),
                Click.on(LOGIN_BUTTON)
        );
    }
}

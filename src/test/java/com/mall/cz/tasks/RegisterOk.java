package com.mall.cz.tasks;

import net.bytebuddy.utility.RandomString;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import static com.mall.cz.pobjects.RegistrationPage.FIRST_NAME_INPUT;
import static com.mall.cz.pobjects.RegistrationPage.LAST_NAME_INPUT;
import static com.mall.cz.pobjects.RegistrationPage.EMAIL_INPUT;
import static com.mall.cz.pobjects.RegistrationPage.PHONE_INPUT;
import static com.mall.cz.pobjects.RegistrationPage.PASSWORD_INPUT;
import static com.mall.cz.pobjects.RegistrationPage.PASSWORD_CONFIRMATION_INPUT;
import static com.mall.cz.pobjects.RegistrationPage.AGREE_CHECKBOX;
import static com.mall.cz.pobjects.RegistrationPage.CONTINUE_BUTTON;

public class RegisterOk implements Task {

    public static RegisterOk called() {
        return Instrumented.instanceOf(RegisterOk.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        String email = RandomString.make(8);
        actor.attemptsTo(
                Enter.theValue("Foo").into(FIRST_NAME_INPUT),
                Enter.theValue("Bar").into(LAST_NAME_INPUT),
                Enter.theValue(email + "@gmail.com").into(EMAIL_INPUT),
                Enter.theValue("777888999").into(PHONE_INPUT),
                Enter.theValue("mypass123").into(PASSWORD_INPUT),
                Enter.theValue("mypass123").into(PASSWORD_CONFIRMATION_INPUT),
                Click.on(AGREE_CHECKBOX),
                Click.on(CONTINUE_BUTTON)
        );
    }
}

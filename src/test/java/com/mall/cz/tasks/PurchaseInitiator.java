package com.mall.cz.tasks;

import com.mall.cz.pobjects.IphonePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class PurchaseInitiator implements Task {

    IphonePage iphonePage;

    public static PurchaseInitiator iphonePage() {
        return instrumented(PurchaseInitiator.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(iphonePage)
        );
    }
}

package com.mall.cz.pobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class ChangeAccountInfoPage extends PageObject {

    public static final Target FIRST_NAME_INPUT = Target
            .the("firstname")
            .locatedBy("//*[@id=\"input-firstname\"]");

    public static final Target LAST_NAME_INPUT = Target
            .the("lastname")
            .locatedBy("//*[@id=\"input-lastname\"]");

    public static final Target EMAIL_INPUT = Target
            .the("email")
            .locatedBy("//*[@id=\"input-email\"]");

    public static final Target PHONE_INPUT = Target
            .the("telephone")
            .locatedBy("//*[@id=\"input-telephone\"]");

    public static final Target CONFIRM_BUTTON = Target
            .the("confirm")
            .locatedBy("//*[@id=\"content\"]/form/div/div[2]/input");

    public static final Target WARNING_VALIDATION_CONTAINER = Target
            .the("confirm")
            .locatedBy("//*[@id=\"content\"]/form/fieldset/div[1]/div/div");
}

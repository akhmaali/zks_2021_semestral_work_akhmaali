package com.mall.cz.pobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class ChangePasswordPage extends PageObject {

    public static final Target PASSWORD_INPUT = Target
            .the("password")
            .locatedBy("//*[@id=\"input-password\"]");

    public static final Target REPEAT_PASSWORD_INPUT = Target
            .the("repeat-password")
            .locatedBy("//*[@id=\"input-confirm\"]");

    public static final Target CONTINUE_BUTTON = Target
            .the("continue")
            .locatedBy("//*[@id=\"content\"]/form/div/div[2]/input");
}

package com.mall.cz.pobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class ChangeAddressPage extends PageObject {

    public static final Target FIRST_NAME_INPUT = Target
            .the("firstName")
            .locatedBy("//*[@id=\"input-firstname\"]");

    public static final Target LAST_NAME_INPUT = Target
            .the("lastName")
            .locatedBy("//*[@id=\"input-lastname\"]");

    public static final Target ADDRESS_INPUT = Target
            .the("address")
            .locatedBy("//*[@id=\"input-address-1\"]");

    public static final Target CITY_INPUT = Target
            .the("city")
            .locatedBy("//*[@id=\"input-city\"]");

    public static final Target POST_CODE_INPUT = Target
            .the("postCode")
            .locatedBy("//*[@id=\"input-postcode\"]");

    public static final Target CONFIRM_BUTTON = Target
            .the("confirm")
            .locatedBy("//*[@id=\"content\"]/form/div/div[2]/input");
}

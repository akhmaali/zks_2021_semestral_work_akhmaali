package com.mall.cz.pobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class MyAccountPage extends PageObject {

    public static final Target EDIT_ACCOUNT_BUTTON = Target
            .the("edit")
            .locatedBy("//*[@id=\"column-right\"]/div/a[2]");

    public static final Target EDIT_ADDRESS_BUTTON = Target
            .the("edit")
            .locatedBy("//*[@id=\"content\"]/ul[1]/li[3]/a");

    public static final Target ADD_ADDRESS_BUTTON = Target
            .the("add")
            .locatedBy("//*[@id=\"content\"]/div/div[2]/a");

    //*[@id="content"]/div/div[2]/a

    public static final Target ACCOUNT_INFO_UPDATED_MESSAGE_CONTAINER = Target
            .the("edit")
            .locatedBy("//*[@id=\"account-account\"]/div[1]");

    public static final Target CHANGE_PASSWORD_BUTTON = Target
            .the("edit")
            .locatedBy("//*[@id=\"column-right\"]/div/a[3]");
}

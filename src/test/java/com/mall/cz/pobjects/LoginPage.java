package com.mall.cz.pobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://demo.opencart.com/index.php?route=account/login")
public class LoginPage extends PageObject {

    public static final Target EMAIL_INPUT = Target
            .the("email")
            .locatedBy("//*[@id=\"input-email\"]");

    public static final Target PASSWORD_INPUT = Target
            .the("password")
            .locatedBy("//*[@id=\"input-password\"]");

    public static final Target LOGIN_BUTTON = Target
            .the("login")
            .locatedBy("//*[@id=\"content\"]/div/div[2]/div/form/input");

    public static final Target INCORRECT_CREDENTIALS_MESSAGE_CONTAINER = Target
            .the("password")
            .locatedBy("//*[@id=\"account-login\"]/div[1]");

    public static final Target LOGIN_CONFIRMATION_ATTRIBUTE = Target
            .the("password")
            .locatedBy("//*[@id=\"content\"]/h2[2]");
}

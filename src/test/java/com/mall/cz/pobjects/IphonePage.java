package com.mall.cz.pobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://demo.opencart.com/index.php?route=product/product&path=24&product_id=40")
public class IphonePage extends PageObject {

    public static final Target ADD_TO_CART_BUTTON = Target
            .the("cart")
            .locatedBy("//*[@id=\"button-cart\"]");

    public static final Target CART_SUMMARY = Target
            .the("cartSummary")
            .locatedBy("//*[@id=\"cart-total\"]");

}

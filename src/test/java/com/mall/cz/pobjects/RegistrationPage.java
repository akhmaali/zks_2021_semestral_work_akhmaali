package com.mall.cz.pobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://demo.opencart.com/index.php?route=account/register")
public class RegistrationPage extends PageObject {

    private static final String XPATH_INPUT_PREFIX = "//input";

    private static final String XPATH_ID_SELECTOR = "@id";

    public static final Target FIRST_NAME_INPUT = Target
            .the("firstname")
            .locatedBy(XPATH_INPUT_PREFIX + "[" + XPATH_ID_SELECTOR + "=\"input-firstname\"]");

    public static final Target LAST_NAME_INPUT = Target
            .the("lastname")
            .locatedBy(XPATH_INPUT_PREFIX + "[" + XPATH_ID_SELECTOR + "=\"input-lastname\"]");

    public static final Target EMAIL_INPUT = Target
            .the("email")
            .locatedBy(XPATH_INPUT_PREFIX + "[" + XPATH_ID_SELECTOR + "=\"input-email\"]");

    public static final Target PHONE_INPUT = Target
            .the("telephone")
            .locatedBy(XPATH_INPUT_PREFIX + "[" + XPATH_ID_SELECTOR + "=\"input-telephone\"]");

    public static final Target PASSWORD_INPUT = Target
            .the("password")
            .locatedBy(XPATH_INPUT_PREFIX + "[" + XPATH_ID_SELECTOR + "=\"input-password\"]");

    public static final Target PASSWORD_CONFIRMATION_INPUT = Target
            .the("confirm")
            .locatedBy(XPATH_INPUT_PREFIX + "[" + XPATH_ID_SELECTOR + "=\"input-confirm\"]");

    public static final Target AGREE_CHECKBOX = Target
            .the("agree")
            .locatedBy("//*[@id=\"content\"]/form/div/div/input[1]");

    public static final Target CONTINUE_BUTTON = Target
            .the("continue")
            .locatedBy("//*[@id=\"content\"]/form/div/div/input[2]");

    public static final Target ACCOUNT_CREATED_HEADER = Target
            .the("success")
            .locatedBy("//div[@id=\"content\"]/h1");

    public static final Target PRIVACY_POLICY_FAIL_MESSAGE_CONTAINER = Target
            .the("confirm")
            .locatedBy(".alert.alert-danger.alert-dismissible");

    public static final Target REGISTRATION_OK_CONTAINER = Target
            .the("Warning")
            .locatedBy("//input[@value = 1]");

    public static final Target REGISTRATION_NOK_CONTAINER = Target
            .the("Warning")
            .locatedBy("//input[@value = 0]");
}

package com;

import com.mall.cz.tasks.RegisterCombParameters;
import com.mall.cz.tasks.RegistrationInitiator;
import cz.cvut.fel.still.sqa.seleniumStarterPack.config.DriverFactory;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.hasItem;
import static com.mall.cz.questions.RegisterCombOkQuestion.theRegCombOkMessage;
import static com.mall.cz.questions.RegisterCombNokQuestion.theRegCombNokMessage;


@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom(value = "registration.csv")
public class ParamRegisterTest {

    private String fistName;
    private String lastName;
    private String email;
    private String phone;
    private String password;
    private boolean ok;

    private WebDriver theBrowser;

    Actor ali = Actor.named("Ali");

    private String OK = "";

    private String NOK = "";

    @Before
    public void before() throws IOException {
        theBrowser = new DriverFactory().getDriver();
        givenThat(ali).can(BrowseTheWeb.with(theBrowser));
    }

    @After
    public void closeBrowser() {
        theBrowser.close();
    }

    public void setFistName(String fistName) {
        this.fistName = fistName.equals("-") ? "" : fistName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName.equals("-") ? "" : lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    @Test
    public void testRegisterCustomer_TwoWay_Ok() {
        givenThat(ali).wasAbleTo(RegistrationInitiator.registrationPage());
        when(ali).attemptsTo(RegisterCombParameters.called(this.fistName, this.lastName, this.email, this.phone, this.password));
        if (this.ok) {
            then(ali).should(seeThat(theRegCombOkMessage(), hasItem(OK)));
        } else {
            then(ali).should(seeThat(theRegCombNokMessage(), hasItem(NOK)));
        }
    }
}

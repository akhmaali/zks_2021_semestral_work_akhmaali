package com;

import com.mall.cz.tasks.ChangeAddress;
import com.mall.cz.tasks.LoginInitiator;
import com.mall.cz.tasks.LoginOk;
import cz.cvut.fel.still.sqa.seleniumStarterPack.config.DriverFactory;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.hasItem;
import static com.mall.cz.questions.AddressChangeOkQuestion.theAddressOkMessage;
import static com.mall.cz.questions.AddressChangeNokQuestion.theAddressNokMessage;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom(value = "change-address.csv")
public class ParamChangeAddressTest {

    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String postCode;
    private boolean ok;

    private WebDriver theBrowser;

    Actor ali = Actor.named("Ali");

    private String OK = "";

    private String NOK = "";


    @Before
    public void before() throws IOException {
        theBrowser = new DriverFactory().getDriver();
        givenThat(ali).can(BrowseTheWeb.with(theBrowser));
    }

    @After
    public void closeBrowser() {
        theBrowser.close();
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName.equals("-") ? "" : firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName.equals("-") ? "" : lastName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    @Test
    public void testChangeAddress_ThreeWay_Ok() {
        givenThat(ali).wasAbleTo(LoginInitiator.loginPage());
        givenThat(ali).wasAbleTo(LoginOk.called());
        when(ali).attemptsTo(ChangeAddress.called(this.firstName, this.lastName, this.address, this.city, this.postCode));
        if (!this.ok) {
            then(ali).should(seeThat(theAddressNokMessage(), hasItem(NOK)));
        } else {
            then(ali).should(seeThat(theAddressOkMessage(), hasItem(OK)));
        }
    }
}
